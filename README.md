# AgroTeconecta<br />

El Ministerio de Agricultura y Desarrollo Rural dispone al público la aplicación Agroteconecta, como una herramienta que permite a los productores la consulta de los principales trámites del sector Agropecuario, para el desarrollo de sus actividades económicas y certificaciones de productos agrícolas.<br />



## Plugins<br />
Agregar los siguientes plugins<br />
cordova plugin add  cordova-plugin-console<br />
cordova plugin add  cordova-plugin-device<br />
cordova plugin add  cordova-plugin-dialogs<br />
cordova plugin add  cordova-plugin-splashscreen<br />
cordova plugin add  cordova-transport-security<br />
cordova plugin add  cordova-plugin-whitelist<br />
cordova plugin add  cordova-plugin-x-socialsharing<br />
cordova plugin add  ionic-plugin-keyboard<br />
cordova plugin add  phonegap-plugin-push<br />


## Plataforma<br />
Plataformas soportadas android, ios wp8<br />

cordova platform add android<br />
cordova platform add ios<br />
cordova platform add wp8