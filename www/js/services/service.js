    angular.module('proyect.service', [] ) 
    // se creo factori para la manupulacion de datos para que
    // funcione se inyecta el nombre en los controladores

    .factory('InfoService', ['$window', '$http','$ionicLoading', '$cordovaDialogs',function(
        $window,
        $http, 
        $ionicLoading,
        $cordovaDialogs, 
        $ionicPlatform)

    {          
        
        var isIOS = ionic.Platform.isIOS();
        var isAndroid = ionic.Platform.isAndroid();
        var isWindowsPhone = ionic.Platform.isWindowsPhone();
        var device = ""
        var tokenPushService = ""
        var tokenUser = ""
        
        if (isIOS){ device = "I"
        }else  if (isAndroid){
            device = "A"
        }else if (isWindowsPhone){
            device = "W"
        }

        

        //var urlServices = "http://186.155.199.197:8000/api/AgroTeConecta/"
        var urlServices = "http://serviciosmadr.minagricultura.gov.co/saviawebapi/api/AgroTeConecta/"
        //var urlServices = "http://104.196.130.110/saviaWebApi/api/AgroTeConecta/";

        
		
        var forgetPasswordUrl   =   urlServices + "RecoveryPass"  
        var registreUrl         =   urlServices + "Registro"        
        var getProfileUrl       =   urlServices + "ObtenerPerfil"        
        var initSessionUrl      =   urlServices + "IniciarSesion"
        var articleYesUrl       =   urlServices + "MarcarComoUtil"
        var moreInformationUrl  =   urlServices + "MasInformacion"     
        var deleteFavoriteUrl   =   urlServices + "QuitarFavorito"  
        var addFavoriteUrl      =   urlServices + "AgregarFavorito"
        var itemViewUrl         =   urlServices + "MarcarConsultado"
        var upLoadUserUrl       =   urlServices + "ActualizarPerfil"
        var getCityUrl          =   urlServices + "ObtenerMunicipios"        
        var allArticleUrl       =   urlServices + "ConsultaArticulos"
        var changePasswordUrl   =   urlServices + "CambiarContrasena"     
        var getFavoriteUrl      =   urlServices + "ConsultaFavoritos"            
        var getListProfileUrl   =   urlServices + "ObtenerListasPerfil"

        var customObjecData
        
        
        


        var req = {
            method: 'POST',
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            timeout : 5000, 
            headers : {'Content-Type': 'application/x-www-form-urlencoded'} 

        }

       


        
      

        //Incio
        var arrayDataT = [];
        var arrayDataN = [];
        var arrayDataP = [];
        var arrayDataAll = [];

        //Favoritos
        var favoriteAll = []
        var favoriteT = []
        var favoriteP = []
        var favoriteN = []    
        var arraySearch = [];
        var currentObj = {}
        var userData = {}




        function loadDB(){

            userData = angular.fromJson($window.localStorage['dataUser'] || "{}") 
            console.log("User db")
            console.log(userData)
        }



        


        // Categorias del LocalStorage Favoritos
        function selectArrayCategoryFavorite(object){        

            console.log(object.category)
            switch(object.category) {

            case "Tramite":
                favoriteT.push(object)
                break;

            case "Programa":
                favoriteP.push(object)
                break;

            case "Noticia":
                favoriteN.push(object)
                break;

            default:
                alert("No existe")
                break;
            }
        }



        var insertFavoriteData = function(object){

            console.log("insertando favoritos")            
            favoriteAll.push(object)
            createLocalStoregeFavorites()

            switch(object.category) {

            case "Tramite":
                favoriteT.push(object)
                break;

            case "Programa":
                favoriteP.push(object)
                break;

            case "Noticia":
                favoriteN.push(object)
                break;

            default:
                alert("No existe")
                break;
            }
        }

            
        var deteleObjectFavorite = function(object){

            for (var i = 0; i < favoriteAll.length; i++){
                if (object.idObjec == favoriteAll[i].idObjec){
                    updateDataDelete(object.idObjec)
                    favoriteAll.splice(i,1);                
                }
            }
            
            switch(object.category) {

            case "Tramite":
                   
                    for (var i = 0; i < favoriteT.length; i++){
                        if (object.idObjec == favoriteT[i].idObjec){
                            favoriteT.splice(i,1);                       
                            return
                         }
                    }

                break;

            case "Programa":
                    for (var i = 0; i < favoriteP.length; i++){
                        if (object.idObjec == favoriteP[i].idObjec){
                            favoriteP.splice(i,1);
                            return
                         }
                    }
                break;

            case "Noticia":
                    for (var i = 0; i < favoriteN.length; i++){
                        if (object.idObjec == favoriteN[i].idObjec){
                            favoriteN.splice(i,1);
                            return
                         }
                    }
                break;

            default:
                alert("No existe")
                break;
            }

            createLocalStoregeFavorites()
        }


        function createUserDB(user){
            $window.localStorage['dataUser'] = angular.toJson(user) 
        }

        function deleteDB(){
            console.log("Eliminar")
            userData = {}
            favoriteAll = []
            favoriteT = []
            favoriteP = []
            favoriteN = []
            
            $window.localStorage.removeItem('dataUser');
            $window.localStorage.removeItem('favortieData');
        }

            
        function createLocalStoregeFavorites(){
            $window.localStorage.removeItem('favortieData');
            $window.localStorage['favortieData'] = angular.toJson(favoriteAll)   
        }

        function recoverFavoritesLocalStorege(){
            return true
        }

        // Buscar resultados

        function resultSearch(){
            return arraySearch
        }

        function objectById(idObjec){
            for (  var i = 0;  i<arrayDataAll.length; i++ ) {

                if ( idObjec == arrayDataAll[i].idObjec) {

                    currentObj = arrayDataAll[i]

                    return arrayDataAll[i]
                }
            }
            return undefined;
        }





      


        //Services Return

        return {

            getDataT: function(){
                return arrayDataT;
            },

            getDataP: function(){
                return arrayDataP;
            },

            getDataN: function(){
                return arrayDataN;
            },

            getDataAll: function(){
                return arrayDataAll;
            },

            getObjectById: function(idObjec){
                
                return objectById(idObjec)
            },

            //Favoritos
            getFavoriteT: function(){
                return favoriteT;
            },

            getFavoriteP: function(){
                return favoriteP;
            },

            getFavoriteN: function(){
                return favoriteN;
            },

            getFavoriteAll: function(){
                return favoriteAll;
            },


            customObjec: function(obj){
                customObjecData = obj
            },

            getCustomObject: function(){
                return customObjecData
            },

            insertFavorite: function(object){   

                insertFavoriteData(object)     

                var data = {
                    Id: object.idObjec,
                    Estado: true,
                    Token: this.getTokenUser()
                }         

                req.url = addFavoriteUrl 
                req.data = data
                $http(req).then(function(response){                                
                    console.log(response.data) 
                })
            },

            deleteFavorite: function(object){
                deteleObjectFavorite(object)
                var data = {
                    Id: object.idObjec,
                    Estado: true,
                    Token: this.getTokenUser()
                }
                req.url = deleteFavoriteUrl 
                req.data = data
                $http(req).then(function(response){                                
                    console.log(response.data) 
                })
            },

            // LocalStore
            createDataUser: function(userLocal){         
                createUserDB(userLocal)
            },

            getDataUser: function(){       
                return userData;
            },

            getResultForSearch: function(){
                return resultSearch()
            },

            deleteDB: function(){
                deleteDB();
            },

            registreUser : function(object){
                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                }); 
                req.url = registreUrl 
                req.data = object
                
                userData.Correo = object.Correo
                userData.Clave = object.Clave

                return  $http(req).then(function successCallback(response){ 
                    $ionicLoading.hide()    

                    return response.data
                }, function errorCallback(response) {
                 
                    $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok').then(function() {
                        console.log("button Ok")
                    });
                    $ionicLoading.hide()
                }) 
            },

            loginUser : function(object){
                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                });

                object.Device = device
                req.url = initSessionUrl 
                object.TokenPush = tokenPushService
                object.Device = device
                req.data = object
                

                return  $http(req).then(function successCallback(response){ 
                    userData.token = response.data.Token;
                    $window.localStorage['tokenUser'] = response.data.Token;
                    console.log(response.data)
                    $ionicLoading.hide()                             
                    return response.data
                }, 

                function errorCallback(response) {
                    
                    $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok').then(function() {
                          console.log("button Ok")
                        });
                    $ionicLoading.hide()
                })                
            },

            forgetPassword: function(email){
                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                }); 
                req.url = forgetPasswordUrl 
                req.data = email
                var state = false
                return  $http(req).then(function successCallback(response){ 
                    $ionicLoading.hide()                                
                    state = true
                    return response.data
                }, function errorCallback(response) {
                 console.log(response)
                $cordovaDialogs.alert(
                    'Asegúrese de estar conectado a internet ', 
                    'Error Conexión', 
                'Ok').then(function() {
                      console.log("button Ok")
                });
                $ionicLoading.hide()
                })   
            },

            getMoreInformation: function(object){
                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                });
                var state = false
              
                req.url = moreInformationUrl 
                object.Id = currentObj.idObjec
                req.data = object

                return  $http(req).then(function successCallback(response){ 
                    state = true 
                    $ionicLoading.hide()                              
                    return response.data
                }, function errorCallback(response) {
                    console.log(response)
                    $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok').then(function() {
                          console.log("button Ok")
                    });
                    $ionicLoading.hide()
                })   

            },

            getToken: function(token){                
                tokenPushService = token
            },
            getTokenUser: function(){
                return $window.localStorage['tokenUser'];


            },

            getProfile: function(){

                req.url = getProfileUrl
                var data = {
                    Token: userData.token 
                }
                req.data = data
                console.log(data)
                tokenUser = data.UserId

                console.log(tokenUser)


                return  $http(req).then(function successCallback(response){ 
                    
                    $ionicLoading.hide()   
                    console.log("dataos usuario")
                    
                    var info = response.data.Data
                    recoverUserInfo(info)
               

                 
                    

                    return response.data

                }, function errorCallback(response) {
                    console.log(response)
                    $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok').then(function() {
                          console.log("button Ok")
                    });
                    $ionicLoading.hide()
                }) 
            },


            // Marcar como visto
            viewArticle: function(){

                var object = {
                    Token : userData.token,
                    Id: currentObj.idObjec
                }
                var state = false
              
                req.url = itemViewUrl
                req.data = object
                $http(req).then(function(response){ 
                    state = true    
                    console.log(response.data)
                                 
                })

            },

            getAllArticle : function(object){ 
            
                arrayDataT = [];
                arrayDataN = [];
                arrayDataP = [];
                arrayDataAll = [];   
                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                });     
                req.url = allArticleUrl  
                req.data = object
                
               
                $http(req).then(function successCallback(response){                     
                
                    selectArrayCategory(response.data,1)
                    $ionicLoading.hide()
                }, function errorCallback(response) {
                    console.log(response)
                     $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok')

                    .then(function() {
                      console.log("button Ok")
                    });
                    $ionicLoading.hide()
                })
            },

            getAllArticleSearch : function(object){

                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                });     

                req.url = allArticleUrl  
                req.data = object
                
              
                return  $http(req).then(function successCallback(response){ 
                    if (response.data.Data.length > 0){
                        selectArrayCategory(response.data,2)                    
                        $ionicLoading.hide()
                        return true
                    }
              
                    $ionicLoading.hide()
                    return false
                }, function errorCallback(response) {
                        
                        console.log(response)
                        $cordovaDialogs.alert(
                            'Asegúrese de estar conectado a internet ', 
                            'Error Conexión', 
                        'Ok').then(function() {
                              console.log("button Ok")
                        });
                        $ionicLoading.hide()
                        return false
                })   
            },

            getFavoriteData: function(){

                loadDB();
                
                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                });     



                var dataObj = {
                    Token : this.getTokenUser() 
                }
                req.url = getFavoriteUrl  
                req.data =  dataObj
                
                
                return  $http(req).then(function successCallback(response){ 
                    console.log("favoritos")
                    console.log(response.data)


                    if (response.data.Estado == 1){
                        
                        if (response.data.Data.length > 0){
                            console.log("cargando desde web")
                            for (var i = 0; i< response.data.Data.length; i++){
                                recoverFavorite(response.data.Data[i])  
                                                              
                            }                                        
                            $ionicLoading.hide()
                            createLocalStoregeFavorites()
                            return true
                        }
                    }else{
                        console.log("cargando desde db")
                        var favoriteAll = angular.fromJson($window.localStorage['favortieData'] || "[]") 

                        for (var i = 0; i < favoriteAll.length; i++){        
                            selectArrayCategoryFavorite(favoriteAll[i])

                        } 
                    }  
                    $ionicLoading.hide()
                    return false
                }, function errorCallback(response) {
                    console.log(response)
                    $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok').then(function() {
                            console.log("button Ok")
                        });
                    $ionicLoading.hide()

                    return false
                })   

               
            },

            articleOk:function(state){
             
                var obj = {
                    Token  : userData.token,
                    Id : currentObj.idObjec,
                    Estado : state
                }              
          
                req.url = articleYesUrl
                req.data = obj
                $http(req).then(function(response){  
                    console.log(response.data)                  

                })
            },

            getMuniciopios:function(object){          
                
                req.url = getCityUrl            
                req.data = object
                return $http(req).then(function(response){ 
                  console.log(response.data)             
                    return response.data
                },function errorCallback(response) {
                    console.log(response)
                    $cordovaDialogs.alert(
                        'Asegúrese de estar conectado a internet ', 
                        'Error Conexión', 
                        'Ok').then(function() {
                            console.log("button Ok")
                        });
                    $ionicLoading.hide()
                    return false
                })
            },


            upLoadUser: function(object){
                console.log(object)
                req.url = upLoadUserUrl 
                req.data = object       

                $ionicLoading.show({
                    templateUrl: 'templates/loader.html',            
                });            
                return $http(req).then(function(response){ 
                    console.log(response.data)   
                    $ionicLoading.hide()      
                    return response.data
                })

            },

            getProfileList:function(object){                
                req.url = getListProfileUrl                
                return $http(req).then(function(response){  
                    console.log(response.data)           
                    return response.data
                })
            },

            changePasswordData:function(object){
                console.log(userData)
                req.url = changePasswordUrl
                object.UserId = userData.token  

                req.data = object
                console.log(object)
                return $http(req).then(function(response){ 
                    state = true 
                    return response.data
                })
            }

        }




        
        function selectArrayCategory(data,type){

            arraySearch = [];
            favoriteAll = [];

            var object = data.Data  
            angular.forEach(object, function(data) {            

                switch(data.TipoArticulo) {

                    

                case 1:

                    
                    var dataTramite = {     
                        category : "Tramite",
                        letterMin: "T",               
                        background: "colorTramite",
                        colorButton: "button-tramite",
                        letterColor: "colorTramiteFont", 
                        stateFavorite: false,
                        iconFavorite: "ion-ios-star-outline",
                        textStateFavorite: "Añadir a favoritos"   
                    }

                    
                    dataTramite.idObjec = data.Id
                    dataTramite.title = data.Titulo
                    dataTramite.name = data.Titulo
                    dataTramite.dataTramite = data.Titulo
                    dataTramite.responsible = data.Responsable
                    dataTramite.createDate = data.FechaCreacion
                    dataTramite.documentRequired = data.Data.DocumentosRequeridos                    
                    dataTramite.content = data.Data.InformacionGeneral
                    dataTramite.afterInformation = data.Data.PasosASeguir
                    dataTramite.contactEmail = data.Data.Contacto.Correo
                    dataTramite.contactUser = data.Data.Contacto.NombreContacto
                    dataTramite.contactPhone = data.Data.Contacto.Telefono
                    dataTramite.contactTime =data.Data.Contacto.Horario    

                    if (type == 1){
                        arrayDataT.push(dataTramite)
                        arrayDataAll.push(dataTramite)
                    }else if(type == 2){
                        arraySearch.push(dataTramite)
                    } else if(type == 3) {
                        console.log(dataTramite)
                        insertFavoriteData(dataTramite)
                    }               

                   

                    break;

                case 2:

       
                    var dataProgram = {     
                        category : "Programa",
                        letterMin: "P",       
                        background: "colorProgram",
                        colorButton: "button-program",
                        letterColor: "colorProgramFont",
                        stateFavorite: false,
                        iconFavorite: "ion-ios-star-outline",
                        textStateFavorite: "Añadir a favoritos"           
                    }

                    dataProgram.idObjec = data.Id
                    dataProgram.title = data.Titulo
                    dataProgram.name = data.Titulo
                    dataProgram.responsible = data.Responsable
                    dataProgram.createDate = data.FechaCreacion
                    dataProgram.timeEnd = data.Data.Vigenciahasta
                    dataProgram.timeStart = data.Data.VigenciaDesde
                    dataProgram.enteResponible = data.Data.EnteResponsable
                    dataProgram.Benefit = data.Data.Beneficio
                    dataProgram.Coverage = data.Data.Cobertura
                    dataProgram.url = data.Data.Enlace
                    dataProgram.content = data.Data.InformacionGeneral
                    dataProgram.point = data.Data.PuntosDeAtencion
                    dataProgram.required = data.Data.RequisitoAplica

                    if (type == 1){
                        arrayDataP.push(dataProgram)
                        arrayDataAll.push(dataProgram)
                    }else if(type == 2){
                        arraySearch.push(dataProgram)
                    }else if(type == 3) {
                        insertFavoriteData(dataProgram)
                    } 

                   


                    break;

                case 3:
                    
                    
                    var dataNotice = {      
                        category : "Noticia",
                        letterMin: "N",                
                        background: "colorNotice",
                        colorButton: "button-notice",
                        letterColor: "colorNoticeFont",
                        stateFavorite: false,
                        iconFavorite: "ion-ios-star-outline",
                        textStateFavorite: "Añadir a favoritos"    
                    }
                    dataNotice.idObjec = data.Id
                    dataNotice.title = data.Titulo
                    dataNotice.name = data.Titulo
                    dataNotice.responsible = data.Responsable
                    dataNotice.createDate = data.FechaCreacion
                    dataNotice.autor = data.Data.Autor
                    dataNotice.content = data.Data.TextoNoticia
                    dataNotice.categoryText = data.Data.Categoria

                    if (type == 1){
                        arrayDataN.push(dataNotice)
                        arrayDataAll.push(dataNotice)
                    }else if(type == 2){
                        arraySearch.push(dataNotice)
                    }else if(type == 3) {
                        insertFavoriteData(dataNotice)
                    }              
                    
                    break;


                default:
                    console.log("Nokkk existe")
                    break;
                }
            })
           
        }

        function recoverFavorite(data) { 
            console.log("data favoritos web")                  
            
            try {
            switch(data.TipoArticulo) {              

                case 1:

                    
                    var dataTramite = {     
                        category : "Tramite",
                        letterMin: "T",               
                        background: "colorTramite",
                        colorButton: "button-tramite",
                        letterColor: "colorTramiteFont", 
                        stateFavorite: true,
                        iconFavorite: "ion-ios-star",
                        textStateFavorite: "Quitar de favoritos"
                    }

                    dataTramite.idObjec = data.Id
                    dataTramite.title = data.Titulo
                    dataTramite.name = data.Titulo
                    dataTramite.dataTramite = data.Titulo
                    dataTramite.responsible = data.Responsable
                    dataTramite.createDate = data.FechaCreacion
                    dataTramite.documentRequired = data.Data.DocumentosRequeridos                    
                    dataTramite.content = data.Data.InformacionGeneral
                    dataTramite.afterInformation = data.Data.PasosASeguir
                    dataTramite.contactEmail = data.Data.Contacto.Correo
                    dataTramite.contactUser = data.Data.Contacto.NombreContacto
                    dataTramite.contactPhone = data.Data.Contacto.Telefono
                    dataTramite.contactTime =data.Data.Contacto.Horario  
                    favoriteT.push(dataTramite)       
                    favoriteAll.push(dataTramite)   
                    updateData(dataTramite.idObjec)                

                    break;

                case 2:

       
                    var dataProgram = {     
                        category : "Programa",
                        letterMin: "P",       
                        background: "colorProgram",
                        colorButton: "button-program",
                        letterColor: "colorProgramFont",
                        stateFavorite: true,
                        iconFavorite: "ion-ios-star",
                        textStateFavorite: "Quitar de favoritos"           
                    }

                    dataProgram.idObjec = data.Id
                    dataProgram.title = data.Titulo
                    dataProgram.name = data.Titulo
                    dataProgram.responsible = data.Responsable
                    dataProgram.createDate = data.FechaCreacion
                    dataProgram.timeEnd = data.Data.Vigenciahasta
                    dataProgram.timeStart = data.Data.VigenciaDesde
                    dataProgram.enteResponible = data.Data.EnteResponsable
                    dataProgram.Benefit = data.Data.Beneficio
                    dataProgram.Coverage = data.Data.Cobertura
                    dataProgram.url = data.Data.Enlace
                    dataProgram.content = data.Data.InformacionGeneral
                    dataProgram.point = data.Data.PuntosDeAtencion
                    dataProgram.required = data.Data.RequisitoAplica

                    favoriteP.push(dataProgram)
                    favoriteAll.push(dataProgram)  
                    updateData(dataProgram.idObjec)                          

            
                    break;

                case 3:
                    
                    
                    var dataNotice = {      
                        category : "Noticia",
                        letterMin: "N",                
                        background: "colorNotice",
                        colorButton: "button-notice",
                        letterColor: "colorNoticeFont",
                        stateFavorite: true,
                        iconFavorite: "ion-ios-star",
                        textStateFavorite: "Quitar de favoritos"    
                    }

                    dataNotice.idObjec = data.Id
                    dataNotice.title = data.Titulo
                    dataNotice.name = data.Titulo
                    dataNotice.responsible = data.Responsable
                    dataNotice.createDate = data.FechaCreacion
                    dataNotice.autor = data.Data.Autor
                    dataNotice.content = data.Data.TextoNoticia
                    dataNotice.categoryText = data.Data.Categoria

                    favoriteN.push(dataNotice)   
                    favoriteAll.push(dataNotice)    
                    updateData(dataNotice.idObjec)         
                    break;


                default:
                    console.log("Nokkk existe")
                    break;
            }

        }

        catch(err) {
            alert(err)
        }

            console.log(favoriteAll)
            console.log(favoriteN)
            console.log(favoriteP)
            console.log(favoriteT)


        }


        function updateData(id){

            console.log(id)

            for (var i = 0; i< arrayDataAll.length; i++){
                if(id == arrayDataAll[i].idObjec){

                    arrayDataAll[i].stateFavorite = true,
                    arrayDataAll[i].iconFavorite = "ion-ios-star",
                    arrayDataAll[i].textStateFavorite =  "Quitar de favoritos" 
                }
            }
        }

         function updateDataDelete(id){

            console.log(id)

            for (var i = 0; i< arrayDataAll.length; i++){
                if(id == arrayDataAll[i].idObjec){

                    arrayDataAll[i].stateFavorite = false,
                    arrayDataAll[i].iconFavorite = "ion-ios-star-outline",
                    arrayDataAll[i].textStateFavorite =  "Añadir a favoritos" 
                }
            }
        }


        function recoverUserInfo(obj){

            console.log("recoverUserInfo: "+JSON.stringify(obj));
            var user = {}
            user.Apellido = obj.Apellido            
            user.Asociacion  = obj.Asociacion
            user.Correo = obj.Correo
            user.Direccion = obj.Direccion
            user.DocIdentidad = obj.DocIdentidad
            user.Genero = obj.Genero         
            user.LugarDeProduccion  = obj.LugarDeProduccion
            user.nitAsociacion = obj.NitAsociacion
            user.NivelEducativoId = obj.NivelEducativo
            user.Nombre = obj.Nombre + " " + obj.SegundoNombre
            user.NumeroCelular = obj.NumeroCelular
            user.PerteneceAsociacion = obj.PerteneceAsociacion == true ? "Si" : "No"
            user.SegundoApellido = obj.SegundoApellido + " " +obj.SegundoNombre            
            user.token = tokenUser            
            user.registre = true  
            user.IdMunicipio = obj.IdMunicipio 
            user.IdActividadAgricola = obj.IdActividadAgricola   
            user.MunicipioName =obj.MunicipioName
            user.DireccionProduccion= obj.DireccionProduccion
                 
            userData = user

            console.log(userData)
            createUserDB(userData)

        }
		
		
    }])
