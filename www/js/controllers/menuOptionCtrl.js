angular.module('proyect.controllers.menuOptionCtrl', [])

.controller('menuOptionCtrl',  function(
    $scope,
    $state,
    $ionicHistory,    
    $ionicSideMenuDelegate,
    
    InfoService)

{



    $scope.goTo = function(tag){

    	if (tag == 1){
    		$state.go('app.user')
    	}
    	if (tag == 2){
            
    		$state.go('app.favorite')
    	}
    	if (tag == 3){
            $state.go('app.login')
    	}   	
        if(tag == 4){
            $state.go('app.info-recientes')
        }
        if(tag == 5){
            $state.go('app.info-popular')
        }
        if(tag == 6){
            $state.go('app.user-changePassword')
        }

        $ionicSideMenuDelegate.toggleRight();
    }

    $scope.closeSesion = function(){
        
        $ionicHistory.clearHistory()
        $ionicHistory.clearCache()
        $state.go('start')
        InfoService.deleteDB()

    } 



     
})