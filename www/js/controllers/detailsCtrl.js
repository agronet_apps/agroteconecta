angular.module('proyect.controllers.detailsCtrl', [])


.controller('detailsCtrl',  function($scope, $state, InfoService, $cordovaSocialSharing,  $cordovaDialogs){


    $scope.dataObject = InfoService.getObjectById($state.params.idObject)   

    InfoService.customObjec($scope.dataObject) 
    $scope.stateButtonYes = 'circleLabelYes'
    $scope.stateButtonNo = 'circleLabelNo'
    $scope.shareButton = 'dataObject.colorButton'
    $scope.viewDataTramite = false
    $scope.viewDataNotice = false
    $scope.viewDataProgram = false

    $scope.buttonYes  = ""
    $scope.buttonNo  = ""


    var user = InfoService.getDataUser()


    $scope.dataObject.read = false

    var changeState = false


    if ($scope.dataObject.category == "Programa"){

        $scope.viewDataTramite = false
        $scope.viewDataNotice = false
        $scope.viewDataProgram = true
        
        var nameProgram = ["Beneficios", "Requisitos para aplicar","Puntos de atención"]
        var itemsProgram = [ $scope.dataObject.Benefit, $scope.dataObject.required,$scope.dataObject.point] 

        $scope.groups = [];

        for (var i = 0; i < 3; i++) {
           
            $scope.groups[i] = {
                name: nameProgram[i],
                items: []
            };
            for (var j = 0; j < 1; j++) {
                $scope.groups[i].items.push(itemsProgram[i]);
            }
        }

    }else if( $scope.dataObject.category == "Tramite") {

        var nameTramite = ["Pasos a seguir", "Documentos requeridos"]
        var itemsTramite = [ $scope.dataObject.afterInformation, $scope.dataObject.documentRequired]

        $scope.viewDataTramite = true
        $scope.viewDataNotice = false
        $scope.viewDataProgram = false

        $scope.groups = [];

        for (var i = 0; i < 2; i++) {
           
            $scope.groups[i] = {
                name: nameTramite[i],
                items: []
            };
            for (var j = 0; j < 1; j++) {
                $scope.groups[i].items.push(itemsTramite[i]);
            }
        }

     }else{
        $scope.viewDataTramite = false       
        $scope.viewDataNotice = true
        $scope.viewDataProgram = false
     }


    var fristType = true

    function timerStart(){
        console.log("prueba")

        if (fristType){ 
            InfoService.viewArticle()  
            fristType = false
        }
        
        return 
    }
    

    setTimeout(timerStart, 4000)

    



    $scope.toggleGroup = function (group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function (group) {
        return $scope.shownGroup === group;
    };   

    $scope.change = function(state) {


        if (user.registre){


            if($scope.dataObject.read){                
                return
            }

            if (state) {

                InfoService.articleOk(true)
                $scope.stateButtonYes = 'circleLabelYes2'
                $scope.stateButtonNo = 'circleLabelNo'
                $scope.dataObject.read = true 
            
            }else{
            
               InfoService.articleOk(false)
                $scope.stateButtonYes = 'circleLabelYes'
                $scope.stateButtonNo = 'circleLabelNo2'
                $scope.dataObject.read = true
            }

            $cordovaDialogs.alert(
                'Tu opinión es muy importante para nosotros', 
                'Gracias', 
                'Ok')

            .then(function() {
                console.log("button Ok")          
            });
                    


        }else{

             $cordovaDialogs.alert(
                'Usuario no registrado, por favor registrate', 
                'Error', 
                'Ok')

            .then(function() {
              console.log("button Ok")
            });

        }

      
        


    }


    $scope.share =  function(color){      

        $scope.shareButton = color
        $cordovaSocialSharing.share(
            "Te invito a conocer más sobre trámites, programas y noticias en AgroTeConecta",
            "Compartir Articulo",
            null,
            "https://play.google.com/store"
        )
    }    
    
})
