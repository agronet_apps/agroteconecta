angular.module('proyect.controllers.searchCtrl', [])

.controller('searchCtrl',  function( 
    $scope,
    $ionicScrollDelegate,
    $state,
    $ionicHistory, 
    InfoService){

	

	$scope.arrayData = InfoService.getResultForSearch()

    $scope.activeBorderColorA = "border-color: #2F6850; background-color: #6abb06; color: white; "
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: ##f2f2f2; color: #59595c; "
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: ##f2f2f2; color: #59595c; "
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: ##f2f2f2; color: #59595c; "

    $scope.actionFavorite = function(object){       

      
        for(var i = 0; i < $scope.arrayData.length; i++){

            if ($scope.arrayData[i].idObjec == object.idObjec) {

                if ($scope.arrayData[i].stateFavorite){
                    //Eliminar
                    $scope.arrayData[i].iconFavorite = "ion-ios-star-outline"
                    $scope.arrayData[i].stateFavorite = false
                    $scope.arrayData[i].textStateFavorite = "Añadir a favoritos"
                    InfoService.deleteFavorite(object) 
                }else{
                    //Crear
                    $scope.arrayData[i].iconFavorite = "ion-ios-star"
                    $scope.arrayData[i].stateFavorite = true
                    $scope.arrayData[i].textStateFavorite = "Quitar de favoritos"
                    InfoService.insertFavorite(object)  
                }
            }
        }  
    }
   

})