angular.module('proyect.controllers.infoCtrl', [])



.controller('infoCtrl',  function(

    $scope,
    $ionicHistory,
    InfoService,
    $ionicScrollDelegate,
    $ionicPopup,
    $cordovaDialogs,    
    $state,
    $stateParams){ 




    
    var data = {
        Query: "",
        OrderByDate :  $stateParams.orderByDate,
        itemData: $stateParams.itemData
    }    

    console.log(data)

    if(data.itemData){

        if (!data.OrderByDate){
            $scope.titleText = "Más consultados"
        }else{
            $scope.titleText = "Recientes"
        }

    }
    

    

     
    InfoService.getAllArticle(data)
    $scope.arrayData = InfoService.getDataAll()
    $scope.userModel = InfoService.getDataUser()    
    $scope.dataSearch = {}



    $scope.activeBorderColorA = "border-color: #2F6850; background-color: #6abb06; color: white; "
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
 
  
    var arrayDataAll = InfoService.getDataAll()
    var arrayDataT = InfoService.getDataT()
    var arrayDataP = InfoService.getDataP()
    var arrayDataN = InfoService.getDataN()

    //$ionicHistory.clearHistory()
    $ionicHistory.clearCache()

    $scope.actionInfo = function(tag){

        $scope.arrayData = [] 
        if(tag == 1 ) {     
            $scope.arrayData = []
            $scope.arrayData = arrayDataAll
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #6abb06; color: white; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "   


        }
        if(tag == 2 ) {       
            $scope.arrayData = []
            $scope.arrayData = arrayDataT
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            
        }
        if(tag == 3 ) {                
            $scope.arrayData = []
            $scope.arrayData = arrayDataP
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #b954f9; color: white; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            
        }
        if(tag == 4 ) {          

            $scope.arrayData = []
            $scope.arrayData = arrayDataN
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #ff8400; color: white; "
        }   
        $ionicScrollDelegate.scrollTop();
    }


    $scope.actionFavorite = function(object){  

        
        $scope.userModel = InfoService.getDataUser()    

         
        if ($scope.userModel.registre) {

            for(var i = 0; i < $scope.arrayData.length; i++){

                if ($scope.arrayData[i].idObjec == object.idObjec) {

                    if ($scope.arrayData[i].stateFavorite){
                        //Eliminar
                        $scope.arrayData[i].iconFavorite = "ion-ios-star-outline"
                        $scope.arrayData[i].stateFavorite = false
                        $scope.arrayData[i].textStateFavorite = "Añadir a favoritos"
                        InfoService.deleteFavorite(object) 
                        return
                    }else{
                        //Crear
                        $scope.arrayData[i].iconFavorite = "ion-ios-star"
                        $scope.arrayData[i].stateFavorite = true
                        $scope.arrayData[i].textStateFavorite = "Quitar de favoritos"
                        InfoService.insertFavorite(object)  
                        return
                    }
                }
            }  

        }else{

             $cordovaDialogs.alert(
                'Por favor registrate para acceder a favoritos', 
                'Registro', 
                'Ok')

            .then(function() {
                console.log("button Ok")
            });
        }      
    }

	$scope.strip=function strip(html)
		{
		
		   var tmp = document.createElement("DIV");
		   tmp.innerHTML = html;
		   return tmp.textContent || tmp.innerText || "";
		}
    
	$scope.showAlert = function(text) {

        var alertPopup = $ionicPopup.alert({
            title: 'Información',
            template: text
        });
        
        alertPopup.then(function(res) {
            $ionicHistory.goBack(-3);
            $state.go('app.info')
        });
    };

    $scope.doRefresh = function () {
        console.log("reload")        
        $scope.arrayData = InfoService.getDataAll()
        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.searhData = function(){
        
        console.log($scope.dataSearch.text)
        if ( $scope.dataSearch.text == "" || $scope.dataSearch.text == undefined) {

            $cordovaDialogs.alert(
                'El campo no puede estar vacío', 
                'Busqueda', 
                'Ok')

            .then(function() {
              console.log("button Ok")
            });

        }else{
            var dataS = {
                Query: $scope.dataSearch.text,
                OrderByDate :  $stateParams.orderByDate
            }  
            
            InfoService.getAllArticleSearch(dataS).then(function(data){

                if (data){
                    $state.go('app.info-search')
                }else{
                    
                    $cordovaDialogs.alert(
                        'No se encontraron resultados', 
                        'Busqueda', 
                        'Ok')

                    .then(function() {
                      console.log("button Ok")
                    });
                }
            })             
        }
    }    
})


