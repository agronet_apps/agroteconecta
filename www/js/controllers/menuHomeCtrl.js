angular.module('proyect.controllers.menuHomeCtrl', [])
.controller('menuHomeCtrl',  function($scope,$ionicHistory ,InfoService,$state){   


  
    $ionicHistory.clearHistory()
    $ionicHistory.clearCache()


    $scope.message=""
	$scope.user = InfoService.getDataUser()   
    $scope.user.existUser = $scope.user.registre
    $scope.textButton = "Perfil"
    $scope.iconData = "iconConecta"
    $scope.textBack = ""


    if (!$scope.user.Nombre == "" || $scope.user.Nombre == undefined ){
        $scope.message = "Por favor completa tu perfil"
    }else{
        $scope.message = $scope.user.Nombre
    }


    $scope.viewButton = function(){


        var url = $ionicHistory.currentView()
        if(url.url == "/app/terminos"){
            $scope.iconData =  "button icon-left ion-chevron-left button-clear "
            $scope.textBack = "Atrás"
            return false    
        }else{
            $scope.iconData = "iconConecta"
            $scope.textBack = ""
            return true
        }
    }

    $scope.backView = function(){

        var url = $ionicHistory.currentView()
        if(url.url == "/app/terminos"){
            $state.go('start')
        }

    }



    $scope.reloadStateUser = function(){    	
    	reloadData()
    }

    function reloadData(){
    	$scope.user = InfoService.getDataUser() 
    	$scope.existUser = $scope.user.registre  
        $scope.user.existUser = $scope.user.registre
        if (!$scope.user.Nombre == "" || $scope.user.Nombre == undefined ){
         //   $scope.message = "Por favor completa tu perfil"
        }else{
           // $scope.message = $scope.user.Nombre
        }	
    }

	
 	 $scope.shouldLeftSideMenuBeEnabled = function (){
    	return true
    }

    $scope.onGesture = function(){
    	reloadData()
    }


   
    

})
