angular.module('proyect.controllers.favoriteCtrl', [])



.controller('favoriteCtrl',  function($scope,$ionicHistory,InfoService,$ionicScrollDelegate){

    $scope.arrayData = InfoService.getFavoriteAll()

    $scope.activeBorderColorA = "border-color: #2F6850; background-color: #6abb06; color: white; "
    $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: ##f2f2f2; color: #59595c; "
    $scope.activeBorderColorP = "border-color: #6F4A89; background-color: ##f2f2f2; color: #59595c; "
    $scope.activeBorderColorN = "border-color: #bf6402; background-color: ##f2f2f2; color: #59595c; "


    var arrayDataAll = InfoService.getFavoriteAll()
    var arrayDataT = InfoService.getFavoriteT()
    var arrayDataP = InfoService.getFavoriteP()
    var arrayDataN = InfoService.getFavoriteN()

    $scope.countA = arrayDataAll.length 
    $scope.countT = arrayDataT.length 
    $scope.countP = arrayDataP.length 
    $scope.countN = arrayDataN.length  

    


    $scope.actionInfo = function(tag){

        $scope.arrayData = [] 
        if(tag == 1 ) {     
            $scope.arrayData = []
            $scope.arrayData = arrayDataAll
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #6abb06; color: white; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: ##f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: ##f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: ##f2f2f2; color: #59595c; "   
           

        }
        if(tag == 2 ) {       
            $scope.arrayData = []
            $scope.arrayData = arrayDataT
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #5b9bfb; color: white; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            
        }
        if(tag == 3 ) {                
            $scope.arrayData = []
            $scope.arrayData = arrayDataP
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #b954f9; color: white; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #f2f2f2; color: #59595c; "
            
        }
        if(tag == 4 ) {          

            $scope.arrayData = []
            $scope.arrayData = arrayDataN
            $scope.activeBorderColorA = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorT = "border-color: #3e7ad3; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorP = "border-color: #6F4A89; background-color: #f2f2f2; color: #59595c; "
            $scope.activeBorderColorN = "border-color: #bf6402; background-color: #ff8400; color: white; "
            
        }   
        $ionicScrollDelegate.scrollTop();
    }


    $scope.actionFavorite = function(object){       

        for(var i = 0; i < $scope.arrayData.length; i++){

            if ($scope.arrayData[i].idObjec == object.idObjec) {

                if ($scope.arrayData[i].stateFavorite){
                    //Eliminar
                    $scope.arrayData[i].iconFavorite = "ion-ios-star-outline"
                    $scope.arrayData[i].stateFavorite = false
                    $scope.arrayData[i].textStateFavorite = "Añadir a favoritos"
                    InfoService.deleteFavorite(object)                     
                }
            }
        }  
    }

    function deleteFavorite(object){
        //Eliminar del array General
        console.log("Eliminar object interno")

        for (var i = 0; i <  $scope.arrayData.length; i++){
            if (object.idObjec ==  $scope.arrayData[i].idObjec){
                 $scope.arrayData.splice(i,1);
                 arrayDataAll.splice(i,1)           
            }
        }

        switch(object.category) {

        case "Tramite":

                for (var i = 0; i < arrayDataT.length; i++){
                    if (object.idObjec == arrayDataT[i].idObjec){
                        arrayDataT.splice(i,1);
                        return
                     }
                }

            break;

        case "Programa":
                for (var i = 0; i < arrayDataP.length; i++){
                    if (object.idObjec == arrayDataP[i].idObjec){
                        arrayDataP.splice(i,1);
                        return
                     }
                }
            break;

        case "Noticia":
                for (var i = 0; i < arrayDataN.length; i++){
                    if (object.idObjec == arrayDataN[i].idObjec){
                        arrayDataN.splice(i,1);
                        return
                     }
                }
            break;

        default:
            alert("No existe")
            break;
        }


        $scope.countA = arrayDataAll.length 
        $scope.countT = arrayDataT.length 
        $scope.countP = arrayDataP.length 
        $scope.countN = arrayDataN.length
    }
        


})


