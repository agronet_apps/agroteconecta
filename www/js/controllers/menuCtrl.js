angular.module('proyect.controllers.menuCtrl', [])

.controller('menuCtrl', function(
	$scope, 
	$ionicScrollDelegate,
	$ionicHistory,
	$state,
	$ionicSideMenuDelegate,
	$cordovaDialogs, 
	InfoService){

	$scope.stateRegistre = true
	$scope.stateSugesstion = false

	$scope.activeBorderColorR = "border-color: #2F6850; background-color: #6abb06; color: white; "
	$scope.activeBorderColorS = "border-color: #59595c; background-color: #f2f2f2; color: #59595c;"

	$scope.userModel = InfoService.getDataUser();


    $scope.userModel.tokenUser = InfoService.getTokenUser();
	console.log("datos del usuario: "+JSON.stringify($scope.userModel));
 
	$scope.userModel.NumeroCelular =  parseInt($scope.userModel.NumeroCelular)
	//$scope.userModel.Genero == "M" ? $scope.userModel.Genero = "Masculino" : $scope.userModel.Genero = "Femenino"


	$scope.password = {}
	$scope.municipiosdataregister = {}
	$scope.muni = {}
	$scope.isMunicipio = false
	$scope.letter = ""
	
    $scope.muni.title= $scope.userModel.MunicipioName
    $scope.userModel.LugarDeProduccion=$scope.userModel.LugarDeProduccion;

	$scope.userModel.Nombre = $scope.userModel.Nombre== undefined ? "" : $scope.userModel.Nombre
	$scope.userModel.SegundoNombre = $scope.userModel.SegundoNombre == undefined ? "" : $scope.userModel.SegundoNombre
	$scope.userModel.Apellido = $scope.userModel.Apellido == undefined ? "" : $scope.userModel.Apellido
	$scope.userModel.SegundoApellido = $scope.userModel.SegundoApellido == undefined ? "" : $scope.userModel.SegundoApellido
			


    $scope.userModel.Nombre= $scope.userModel.Nombre+" "+$scope.userModel.SegundoNombre+" "+
   					 $scope.userModel.Apellido+" "+$scope.userModel.SegundoApellido;


    $scope.arrayActivity = []
    InfoService.getProfileList().then(function(data){
        

        if (data.Estado == 1 ){
            $scope.arrayActivity = data.Data.ActividadesAgricolas            
            $scope.nivelEducativo =  data.Data.NivelesEducativos;
            

            for (var i = 0; i < $scope.nivelEducativo.length; i++){
            	if($scope.userModel.NivelEducativoId == $scope.nivelEducativo[i].Id){
            		console.log("nivel NivelEducativo1: "+$scope.nivelEducativo[i].Nombre);
            		
            		$scope.userModel.NivelEducativo =  $scope.nivelEducativo[i].Nombre;
            		console.log("nivel NivelEducativo2: "+$scope.userModel.NivelEducativo);
            	}
            }

            console.log($scope.userModel.IdActividadAgricola)
            for (var i = 0; i < $scope.arrayActivity.length; i++){
            	if($scope.userModel.IdActividadAgricola == $scope.arrayActivity[i].Id){
            		
            		$scope.userModel.ActividadAgricola =  $scope.arrayActivity[i].Nombre
            		console.log($scope.userModel.ActividadAgricola)
            	}
            }
        }        
    }) 


	

	$scope.actionButtonRegistre = function(){
		$ionicScrollDelegate.scrollTop();
		$scope.stateRegistre = true
		$scope.stateSugesstion = false
		$scope.activeBorderColorS = "border-color: #2F6850; background-color: #f2f2f2; color: #59595c; "
		$scope.activeBorderColorR = "border-color: #59595c; background-color: #6abb06; color: white; "
		
	}

	$scope.actionButtonSugesstion = function(){
		$ionicScrollDelegate.scrollTop();
		$scope.stateRegistre = false
		$scope.stateSugesstion = true
		$scope.activeBorderColorS = "border-color: #2F6850; background-color: #6abb06; color: white; "
		$scope.activeBorderColorR = "border-color: #59595c; background-color: #f2f2f2; color: #59595c; "
	}	

	$scope.cleanStack = function(form){	
		
		if (form.$valid) {
			

			
			var partName = $scope.userModel.Nombre.split(' ');
			//var partLastName = $scope.userModel.Nombre.split(' ');
			var IdActividadAgricolaData = ""

			for(pt=0;pt<partName.length;pt++)
			{
				if(pt==0){$scope.userModel.Nombre = partName[0] == undefined ? "" : partName[0]}
				if(pt==1){$scope.userModel.SegundoNombre = partName[1] == undefined ? "" : partName[1]}
			    if(pt==2){$scope.userModel.Apellido = partName[2] == undefined ? "" : partName[2]}
				if(pt==3){$scope.userModel.SegundoApellido = partName[3] == undefined ? "" : partName[3]}
			
			}

			

			for (var i = 0 ; i<$scope.arrayActivity.length; i++){
				if ($scope.arrayActivity[i].Nombre == $scope.userModel.ActividadAgricola){
					IdActividadAgricolaData = $scope.arrayActivity[i].Id
				}
			}

			var nivelEducativoId = 0
			for (var i = 0; i<$scope.nivelEducativo.length; i++){
				if ($scope.nivelEducativo[i].Nombre == $scope.userModel.NivelEducativo){
					nivelEducativoId = $scope.nivelEducativo[i].Id
				}
			}
	        $scope.userModel.registre = true	
			var data = {
				Aplicacion: "TeConecta",
				Nombre : $scope.userModel.Nombre,
				SegundoNombre: $scope.userModel.SegundoNombre,
				Apellido: $scope.userModel.Apellido,
				SegundoApellido: $scope.userModel.SegundoApellido,
				UserId : $scope.userModel.tokenUser,				
				DocIdentidad : $scope.userModel.DocIdentidad,				
				NivelEducativo : nivelEducativoId,								
				Correo : $scope.userModel.Correo,
				Direccion: $scope.userModel.Direccion,
                registre: true,
				Genero: $scope.userModel.Genero,//( $scope.userModel.Genero == "Masculino"), ? "M" : "F" ) ,
				NumeroCelular: $scope.userModel.NumeroCelular,
				DireccionProduccion: $scope.userModel.LugarDeProduccion,
				PerteneceAsociacion: ( $scope.userModel.PerteneceAsociacion == "Si" ? true :false ),
				IdMunicipio: 	$scope.userModel.IdMunicipio,
				MunicipioName: 	$scope.muni.title,
				IdActividadAgricola:  IdActividadAgricolaData,
				Asociacion: $scope.userModel.Asociacion,
				IdCategoriaProducto: "12" // no esta definido
			}

			InfoService.upLoadUser(data);
			InfoService.createDataUser(data)
			$ionicHistory.goBack(-3);

		}else{
			$cordovaDialogs.alert(
			    'Verifique los campos', 
			    'Registro', 
			    'Ok')
			.then(function() {
			  	$ionicHistory.goBack(-2);		
			});	
		}
	}

	$scope.changePassword = function(){		
		

		if ( ($scope.password.Actual == "" || $scope.password.Actual==undefined) ||
			($scope.password.Nueva == "" || $scope.password.Nueva==undefined)  ||
			($scope.password.Confirmacion == "" || $scope.password.Confirmacion==undefined) 
		 ){
		 	$cordovaDialogs.alert(
	            'Todos los campos son obligatorios', 
	            'Error', 
	            'Ok')

	        .then(function() {
	          console.log("button Ok")
	        });
   		}else if($scope.password.Nueva  == $scope.password.Confirmacion ){
   		 

   			var obj = {
   				Nueva:$scope.password.Nueva ,
   				Confirmacion:$scope.password.Confirmacion ,
   				Actual:$scope.password.Actual  
   			}


   			//1233@prueba.com pass casas
   			InfoService.changePasswordData(obj).then(function(data){
   				console.log(data)
   				
   				if (data.Estado == 0) {

   					$cordovaDialogs.alert(
			            'No se pudo cambiar la contraseña', 
			            'Error', 
			            'Ok')

			        .then(function() {
			          console.log("button Ok")
			        
			        });

   				}else{
   					$cordovaDialogs.alert(
			            'Se cambio su contraseña correctamente', 
			            'Error', 
			            'Ok')

   					$ionicHistory.goBack(-4);
   				}
   			})

   		}else{
   			$cordovaDialogs.alert(
	            'Las contraseñas no coinciden', 
	            'Error', 
	            'Ok')

	        .then(function() {
	          console.log("button Ok")
	        });

   		}	
	}

	$scope.recoverFavorites = function(){
		
		InfoService.getFavoriteData()
	}

	$scope.searchMunicipios = function(muni) {
   		
   		$scope.letter = muni
   		var data = {
   			query:muni
   		}  		
		InfoService.getMuniciopios(data).then(function(data) {
			$scope.isMunicipio=true									
            $scope.municipiosdataregister.municipiosregister = data.Data;
        }
      )
    }

    $scope.seleccionar = function(item){

      $scope.muni.title=item.Nombre + " - " + item.Departamento;
      municipioLocal = item.Id;
      $scope.userModel.IdMunicipio = municipioLocal
      console.log("SELECTED ID : "+municipioLocal);
      $scope.isMunicipio  = false;
    }

    $scope.changeButton = function(){
        console.log("cliclicci");
    }




})