// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
//ionic resources --icon


//cordova plugin add cordova-plugin-dialogs
//cordova plugin add phonegap-plugin-push

angular.module('proyect', 
  [
    'ionic','ionic.service.core',    
    'proyect.controllers.menuCtrl',
    'proyect.controllers.favoriteCtrl',
    'proyect.controllers.detailsCtrl',
    'proyect.controllers.loginCtrl',
    'proyect.controllers.infoCtrl',
    'proyect.controllers.menuOptionCtrl',
    'proyect.controllers.menuHomeCtrl',
    'proyect.controllers.searchCtrl',
    'proyect.directives',
    'proyect.service',
    'ngCordova'
  ])

//AIzaSyAPQrUPLYXj_hlY3c7hAWRa5D76_l16gp0

.run(function($ionicPlatform,InfoService) {
  $ionicPlatform.ready(function($cordovaPush) {



    var io = Ionic.io();
    var user = Ionic.User.current();

    if (!user.id) {
      user.id = Ionic.User.anonymousId();
    }

   
    pushNotificationAction();
    
    function pushNotificationAction () {

    /*
      pushNotification = window.plugins.pushNotification;

        if ( device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos" )
        {         
          pushNotification.register( successHandler,  errorHandler,
            {
                "senderID":"554273018793",
                "ecb":'window.onAndroidNotification'
            });

        }else {
          pushNotification.register(tokenHandlerIOS,  errorHandlerIOS,
            {
                "badge":"true",
                "sound":"true",
                "alert":"true",
                "ecb": 'window.onAndroidNotification '
            });
        }*/
    }

    function successHandler (result) {  

      console.log(result)
    }


    function errorHandler (error) {
      console.log(result)        
    }

    function successHandlerIOS (result) {  

      console.log(result)
    }


    function errorHandlerIOS (error) {
      console.log(result)        
    }

     function tokenHandlerIOS (result) {  

      user.set('name', Math.random()*10000 );    
      user.set('pushTokent', result);                    
      user.save();                            
      InfoService.getToken(e.regid) 
    }


    function errorHandlerIOS (error) {
      console.log(result)        
    }



    // Android and Amazon Fire OS
    window.onAndroidNotification = function(e)  {
    
      switch( e.event )
      {

        case 'registered':

            if ( e.regid.length > 0 ) {                                
                user.set('name', Math.random()*10000 );    
                user.set('pushTokent', e.regid);               
                user.save();                            
                InfoService.getToken(e.regid)       
            }

        break;

        case 'message':



            user.set('e', e);                
            user.save();   
            
          if ( e.foreground )  {
                        
              var soundfile = e.soundname || e.payload.sound;                
              var my_media = new Media("/android_asset/www/"+ soundfile);
                my_media.play();

          }else{
              if ( e.coldstart ){

                console.log(e.coldstart )

              }else{
                console.log(e.coldstart )                    
              }
          }

            //e.payload.message
            //e.payload.msgcnt
            //e.payload.timeStamp

        break;

        case 'error':
            console.log("error")
        break;

        default:
            console.log("Default")
        break;
      }
    }



    //iOS
    window.onNotificationIOS = function(e)  {


        if ( event.alert ) {
            navigator.notification.alert(event.alert);


        }

        if ( event.sound ) {
            var snd = new Media(event.sound);
            snd.play();
        }

        if ( event.badge ){
            pushNotification.setApplicationIconBadgeNumber(successHandlerIOS, errorHandlerIOS, event.badge);
        }
    }


    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
       StatusBar.overlaysWebView(false);
       StatusBar.hide();
    }

  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.backButton.previousTitleText(false);
  $ionicConfigProvider.backButton.icon('ion-chevron-left');
  $ionicConfigProvider.backButton.text('Atrás')

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'menuHomeCtrl'    
  })

  //home
  .state('app.info', {
    url: '/info',

    views: {
      'menuContent': {
        templateUrl: 'templates/tabs/infoTabs.html',
        controller: 'infoCtrl'        
      }
    }
  })

  .state('app.terminos', {
    url: '/terminos',

    views: {
      'menuContent': {
        templateUrl: 'templates/tabs/terminos.html',
        controller: 'infoCtrl'        
      }
    }
  })

.state('politicasPrivacidad', {
    url: '/politicasPrivacidad',
    templateUrl: 'templates/tabs/politicasPrivacidad.html',
  })

  .state('app.info-recientes', {
    url: '/info',
    params:{
      orderByDate: true,
      itemData:true
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/tabs/infoTabs.html',
        controller: 'infoCtrl'        
      }
    }
  })


  //populares
  .state('app.info-popular', {
    url: '/info/popular',
    params:{
      orderByDate: false,
      itemData:true
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/tabs/infoTabs.html',
        controller: 'infoCtrl'        
      }
    }
  })

  //Detalles
  .state('app.info-detail', {
    url: '/info/detail/:idObject',
    views: {
      'menuContent': {
        templateUrl: 'templates/tabs/details.html',
        controller: 'detailsCtrl'       
      }
    }
  })


   //Solicitar informacion
  .state('app.info-detail-request', {
    url: '/info/detail/:idObject/request',
    views: {
      'menuContent': {
        templateUrl: 'templates/tabs/information.html',
        controller: 'loginCtrl' 
      }
    }
  })


  //User
  .state('app.user', {
    url: '/user',
    views: {
      'menuContent': {
         templateUrl: 'templates/profile/menu.html',
         controller: "menuCtrl"         
      }
    }
  })

  // ChangePassword
  .state('app.user-changePassword', {
    url: '/user/changePassword',
    views: {
      'menuContent': {
         templateUrl: 'templates/profile/changePassword.html',
         controller: "menuCtrl"         
      }
    }
  })


  //Favorite
  .state('app.favorite', {
    url: '/favorite',
    views: {
      'menuContent': {
         templateUrl: 'templates/profile/favorite.html',
         controller: "favoriteCtrl"      
      }
    }
  })


  //Login
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
         templateUrl: 'templates/login/login.html',
         controller: 'loginCtrl'     
      }
    }
  })

  //RecoverPassword
  .state('app.login-recover', {
    url: '/login/recover',
    views: {
      'menuContent': {
         templateUrl: 'templates/login/recoverPassword.html',
         controller: 'loginCtrl'     
      }
    }
  })

  //Result
  .state('app.login-result', {
    url: '/login/result',
    views: {
      'menuContent': {
         templateUrl: 'templates/login/resultLogin.html',
         controller: 'loginCtrl'     
      }
    }
  })

  //Search
  .state('app.info-search', {
    url: '/info/detail/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search/search.html',
        controller: 'searchCtrl'       
      }
    }
  })



  //Init App
  .state('start', {
    url: '/start',
    templateUrl: 'templates/start.html',
    controller: "menuCtrl" 
  })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('start');
});




