 angular.module('proyect.directives', [])


.directive('menuRegistre', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/registre.html'
  };
})

.directive('menuSugesstion', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/sugesstion.html'
  };
})

.directive('cellView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/dataView.html'
  };
})

.directive('profileView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/profile.html',
    controller: 'menuOptionCtrl'
  };
})

.directive('noticeView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/noticeView.html',
    controller: 'detailsCtrl'
  };
})

.directive('tramiteView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/tramiteView.html',
    controller: 'detailsCtrl'
  };
})

.directive('programView', function () {
  return {
    restrict: 'E',
    templateUrl: 'templates/directives/programView.html',
    controller: 'detailsCtrl'
  };
})





