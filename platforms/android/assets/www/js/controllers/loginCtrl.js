angular.module('proyect.controllers.loginCtrl', [])




.controller('loginCtrl',  function( 
		$scope,
		$ionicScrollDelegate,
		$state,
		$ionicHistory,
		$ionicPopup,
		$cordovaDialogs,
		InfoService
		) {

	$scope.existUser = true;
	$scope.textButton = "Crear una cuenta"
	$scope.typeInput = "password"
	$scope.info = {}
	$scope.view = true
	$scope.muni = {}


	$scope.dataObject = InfoService.getCustomObject()

	if($scope.dataObject!=null && $scope.dataObject!='undefined'){
		
		


	if ($scope.dataObject.letterMin == "T"){

		$scope.dataObject.borderColor = "#5b9bfb"

	}else if($scope.dataObject.letterMin == "N"){

		$scope.dataObject.borderColor = "#ff8400"

	}else{

		$scope.dataObject.borderColor = "#6F4A89"
	}

}


	$scope.userModel = InfoService.getDataUser()	
	$scope.titleResutl = "Bienvenido a AgroTeConeta"	
    $scope.registerTitle = true;
	var recoverInfo = false

	var stateViewPassword = true

	$scope.existUserAction = function(){

		$ionicScrollDelegate.scrollTop();
		$scope.registerTitle = false;
		$scope.existUser = false;
		$scope.textButton =  "Ingresar"	
	}

	$scope.goToHome= function(index) {	


		if ( index == 1 ){

			var data = {
				Correo: $scope.info.email,
				ExtraInfo : $scope.info.text,			
			}

			InfoService.getMoreInformation(data).then(function(data){
				
				if (data.Estado ==1){
					$ionicHistory.goBack(-2);					
				}else{

					$cordovaDialogs.alert(
					    'Error en la conexion', 
					    'Error', 
					    'Ok')
					.then(function() {
					  	$ionicHistory.goBack(-2);		
					});
				}
			})

		}else {
			$ionicHistory.goBack(-2);
		}	
			 
	}

	$scope.showAlert = function(text) {
		$cordovaDialogs.alert(
		    'Se ha enviado su solicitud correctamente', 
		    'Información', 
		    'Ok')

		.then(function() {
			$scope.view = false	  			
		});
	};

	$scope.onSubmit = function(form){


		if (form.$valid) {

			if ($scope.existUser){
				recoverInfo = false
				var data = {
					Correo: form.email.$viewValue,
					Clave: form.password.$viewValue
				}				
				InfoService.registreUser(data).then(function(data){	
					console.log(data)	
					if (data.Estado != 0){	
						initSession(form)
					}
					else {	alert(data.Texto)	}				
				})

			}else{	
				initSession(form)	
				recoverInfo = true
			}	

		} else {
			$cordovaDialogs.alert(
			    'Verifique la dirección de correo y que la contraseña tenga minimo 5 caracteres', 
			    'Información', 
			    'Ok')
			.then(function() {
			  			
			});	

		}
	}

	$scope.onSubmitRecover = function(form){

		if (form.$valid) {
			var data = {
				Correo : form.email.$viewValue
			}				

			InfoService.forgetPassword(data).then(function(data){

				if (data.Estado != 0){	$scope.showAlert(data.Texto)	}
				else {
					$cordovaDialogs.alert(
					    'Error en la solicitud, por favor vuelva a intentar', 
					    'Información', 
					    'Ok')
					.then(function() {
					  	$ionicHistory.goBack(-2);		
					});					
				}				
			})

		}else {
			$cordovaDialogs.alert(
			    'Verifique sus datos', 
			    'Información', 
			    'Ok')

			.then(function() {
			  	$ionicHistory.goBack(-2);		
			});		
		}
	}

	$scope.viewPassword = function(){
		if (stateViewPassword) {
			//Ver
			stateViewPassword = false
			$scope.typeInput = "text"
		}else {
			stateViewPassword = true
			$scope.typeInput = "password"
		}
	}


	function initSession(form){

		var data = {
			Email: form.email.$viewValue,
			Password: form.password.$viewValue
		}		

		InfoService.loginUser(data).then(function(data){

			if (data.Estado != 0){

				$scope.userModel.registre = true	
				InfoService.createDataUser($scope.userModel)
				InfoService.getFavoriteData()


				if (recoverInfo){
					recoverInfo = false
					InfoService.getProfile()
				}


				$state.go('app.login-result')
			}else {

				$cordovaDialogs.alert(
			    data.Texto, 
			    'Información', 
			    'Ok')

				.then(function() {
				  	$ionicHistory.goBack(-2);		
				});				
			}				
		})
	}   

	$scope.continueButton = function(){
		$ionicHistory.goBack(-2);
	}
})
